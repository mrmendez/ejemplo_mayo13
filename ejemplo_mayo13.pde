Cadena str;
Boton b1,b2;
PFont p;
String men;
public void setup(){
    size(300,200);
    smooth();
    str = new Cadena("ANITA LAVA LA TINA");
    p = createFont("Arial",18);
    textFont(p);
    b1 = new Boton(20,25,"boton 1");
    b2 = new Boton(100,25,"boton 2");
    men ="Aqui va el mensaje";
}

public void draw(){
     background(127);
     fill(0,255,0);
     text(str.toString(),20,height/2);
     b1.dibujar();
     b2.dibujar();
     fill(255);
     text(men,20,height-30);
}

public void mousePressed(){
    men = b1.click(mouseX, mouseY);
}